import { Injectable } from '@nestjs/common';
import { Item } from './interfaces/item.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { doc } from 'prettier';
import { ItemDto } from './dto/item.dto';

@Injectable()
export class ItemsService {
  constructor(@InjectModel('Item') private readonly itemModel: Model<Item>) {}

  async findAll(): Promise<Item[]> {
    return await this.itemModel.find();
  }

  async findOne(id: string): Promise<Item> {
    return await this.itemModel.findById(id);
  }

  async create(data: ItemDto): Promise<Item> {
    const newItem = new this.itemModel(data);
    return await newItem.save();
  }

  async delete(id: string): Promise<Item> {
    return await this.itemModel.findByIdAndDelete(id);
  }

  async update(id: string, data: ItemDto): Promise<Item> {
    return await this.itemModel.findByIdAndUpdate(id, data, { new: true });
  }
}
