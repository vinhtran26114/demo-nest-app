import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
} from '@nestjs/common';
import { ItemDto } from './dto/item.dto';
import { Item } from './interfaces/item.interface';
import { ItemsService } from './items.service';

@Controller('items')
export class ItemsController {
  constructor(private readonly itemService: ItemsService) {}

  @Get()
  async findAll(): Promise<Item[]> {
    return this.itemService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Item> {
    return this.itemService.findOne(id);
  }

  @Post()
  async createItem(@Body() createItemDto: ItemDto): Promise<Item> {
    return this.itemService.create(createItemDto);
  }

  @Delete(':id')
  async eleteOne(@Param('id') id: string): Promise<Item> {
    return this.itemService.delete(id);
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() request: ItemDto,
  ): Promise<Item> {
    return this.itemService.update(id, request);
  }
}
